import os
from flask import Flask
from . import (
    index,blog,projects,about,contact
)

def create_app(test_config=None):
    # __name__ = current python module
    # arg2 = rel to instance folder
    app = Flask(__name__, instance_relative_config=True)

    if test_config is None:
        app.config.from_pyfile('config.py', silent=True)
    else:
        app.config.from_mapping(test_config)


    # check instance folder
    try:
        os.makedirs(app.instance_path)
    except OSError:
        pass

    app.register_blueprint(index.bp)
    app.add_url_rule('/', endpoint='index')

    app.register_blueprint(blog.bp)
    app.add_url_rule('/blog', endpoint='blog')

    app.register_blueprint(about.bp)
    app.add_url_rule('/about', endpoint='about')

    app.register_blueprint(projects.bp)
    app.add_url_rule('/projects', endpoint='projects')

    app.register_blueprint(contact.bp)
    app.add_url_rule('/contact', endpoint='contact')


    return app

app = create_app()




if __name__ == "__main__":
    app.run(host='0.0.0.0')
