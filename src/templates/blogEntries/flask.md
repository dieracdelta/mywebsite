-------------------------------------------------------------------------------

# My Blog: Entry 1 #

Thought I'd start a blog because I have so much free time now...I'm planning to use this both as a reference for future me and a way for me to chart my productivity over this summer.


Let's start this off with a few words about how I made this website, as it was a bit of a journey to actually get it online. 

This is really my first brush with anything webdev related...I'm definitely more of a backend guy, but I definitely feel like I should at least give it a try. This is pretty much why this website exists.

## Overall Structure ##

So, I'm using flask as my backend framework, a digitalocean Ubuntu VPS as my host, and using a domain I bought off Namecheap. All of this, for free. Github's student pack rocks. Anyway, let's talk about each piece and why I chose it:

### Framework? ###

So, flask is a relatively minimalistic backend framework implemented in python. But why use a framework in the first place? 

Essentially, I had a template for a base page that a bunch of html (e.g. a bar, background, etc) that I didn't really want to copy and paste for every html page I wrote. Flask has this nice inheritance system that allows other pages to *extend* current pages. It also does some nice routing and page modifications (the niceties of jinja) that makes life easier.

Jinja essentially lets you dynamically generate web pages using python syntax. I think that's neat.

So to summarize, Flask is basically python that allows you to (1) organize your webpage in modular fashion, and (2) insert python into your html in a way that allows for dynamic web page generation. Souunds pretty great.

I chose it for three reasons: (1) I already know python (2) I wanted something minimalistic, which is this framework in a nutshell, and (3) it fixed the problems I was having

### Domain Name ###

NameCheap allows you to purchase domain names. You even get one for free (why I chose it) if you're a student and Github sent you. Great, now let's worry about hosting...

### Hosting ###

I was a fan of the whole "free because I'm a student" idea. This presented two main options: digitalocean (I got 10 months free hosting from github) or github pages. Github pages was alright initially, as it supports hosting static webpages. However, once I started using flask, this was no longer possible because I was dynamically generating content.

Flask does allow you to "freeze" your environment to display it statically, but now this felt like I was crossing off user interactivity.

So I switched to DO. It was pretty easy to host *something* with DO. Essentially, all I did was spin up a ubuntu "droplet" (a VPS/virtual private server), add up a few name servers, and I was set. Their tutorial were pretty easy to follow. Sidenote: I kind of wish that we could have more control over the VPS we spun up. There wasn't a gentoo or artix option, which was sad.

The tricky part was hosting a *flask* application. I initially wanted to do this with apache. I realized after a few hours of smashing my head into my keyboard that apache isn't the only option. Apache had a bunch of issues: it was pretty particular about the file structure it expected in the website it was hosting, and the config file syntax it had was pretty poorly documented.

So eventually I realized you could also host a flask app with Gunicorn and Nginx. This was SO much easier. Took me 15 minutes to follow the DO turtorial, and I was set! Amazing. DO should really make that the default...

Anyway, lesson learned...apache is evil. Great.


## Designing the website ##

So this I am admittedly pretty bad at, I think. I wanted the website to have a dark theme, but I have no idea how to make it look decent... I'm actually such trash at css, and I don't know anything about js or html. But let's go through what I've done so far:

### css ###

A few neat things I did with css:

  * Change the cursor to and bullet points with svg images.
  * Add in navigation bars (w3school has a pretty decent turtorial on bars)
  * Add in an animation to the navbars upon selection
  * Make a few circles.

The biggest takeaway from this is css sucks and I'm really not an artist.

### flask + html ###

I threw together a few categories and set up routing to them with flask. Flask does this super generally, which is awesome. 

Flask has a directory structure along the lines of (see my github for *this* website as an example):

  * src
     * templates
     * static
     
So I can put static content in the static folder. And, flask dynamically generates content based off both routing and what you have given it as a html template. You can render a template from the templates directory. But that's not the crazy thing. Templates allow for inheritance. This saves you from copying and pasting the same html code time after time. Ready for change!!

For example, I did this with every webpage in the site. I didn't want to copy and paste in the same div for the nav bar on each page. And so I create a base page "template", then wrote a bunch of html pages that inherited from that basic template. Amazing!

Another neat thing is the routing flask allows you to do. If you get a html request, what you can do is expect what kind of request you would expect it to be via python decorators, then handle it based on the arguments specified both in the request and the decorator. You can then use these arguments (or other arbitrary arguments) when dynamically creating the html!

I used both of these features when creating my blog page. Specifically, I used a python for loop *in* the html to create a bunch of divs, one for each blog entry that python found, and I passed that in as an argument when calling render_template() when a user requests to see the "/blog" page.

### logging + cookies ###

I wanted to see how much information I could glean about the visitors to my site. So the first thing I wanted to do was log all the requests made. This turns out to be pretty easy to do. You have access to the raw request made to the site within flask. So all I needed to do was convert that to a string, and print to a file. Neat things you can plot include IP address, time, page, cookie (if there exists one) etc. Easy.

Beyond this, one of the things you can do is add in a cookie. I tried this to count the number of unique visitors to the site (again, see source). Neat. As far as I can tell, you can't do much more with a cookie, but then again I haven't really looked into this.

### What's next? ###

I want to use this blog as a way to talk about what I'm doing this summer, as it's a convenient way to track my work. 

The website itself will probably just focus on discussion of projects I've worked/am working on...

Beyond that, I don't have many more plans. It's essentially finished. If I have time, I'll fix the css to look less ugly, and maybe add in more animations. Flask was the main highlight!
