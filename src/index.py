from flask import (
    Blueprint, flash, g, redirect, render_template, request, url_for, make_response
)
import datetime
from pytz import timezone
import os
from werkzeug.exceptions import abort

bp = Blueprint('index', __name__)

visitorIndex = 0
requestIndex = 0

def write_log(request):
    is_ok = False
    try:
        size = os.stat("log").st_size
        if size < 1000000000:
            is_ok = True
    except:
        is_ok = True
    # less than a gig to prevent ddos killing memory
    try:
        file  = open("log", "a")
    except:
        file  = open("log", "w")
    file.write("request:\r\n")
    file.write("    " + "- IP Address: " + str(request.remote_addr) + "\r\n")
    file.write("    " + "- Access Route: " + str(request.access_route) + "\r\n")
    file.write("    " + "- Path: " + str(request.path) + "\r\n")
    file.write("    " + "- Cookies: " + str(request.cookies) + "\r\n")
    file.write("    " + "- Date: " + str(datetime.datetime.now(timezone("EST"))) + "\r\n")
    file.close()
    global requestIndex
    requestIndex += 1

def try_add_cookie(page_template):
    if not request.cookies.get('troll'):
            res = make_response(page_template)
            res.set_cookie('troll', str(requestIndex), max_age=63072000)
            global visitorIndex
            visitorIndex += 1
    else:
        # res = make_response("Value of cookie foo is {}".format(request.cookies.get('foo')))
        res = page_template
    return res


@bp.route('/')
@bp.route('/<page>')
def index(**kwargs):
    write_log(request)
    return try_add_cookie(render_template('index.html'))
