from flask import (
    Blueprint, flash, g, redirect, render_template, request, url_for
)
from . import index
import os
from werkzeug.exceptions import abort

bp = Blueprint('contact', __name__)

@bp.route('/contact')
def contact():
    index.write_log(request)
    return index.try_add_cookie(render_template('contact.html'))
