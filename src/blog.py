from flask import (
    Blueprint, flash, g, redirect, render_template, request, url_for
)
from . import index
import os
from werkzeug.exceptions import abort

bp = Blueprint('blog', __name__)

@bp.route('/blog')
def blog():
    index.write_log(request)
    entry_list = get_blog_entry_list()

    if request.args.get('entry') in os.listdir("./src/templates/blogEntries"):
        return index.try_add_cookie(render_template('./blogEntries/' + request.args.get('entry'), entries=entry_list))

    return index.try_add_cookie(render_template('blog.html', entries=entry_list))

def get_blog_entry_list():
    blog_list = os.listdir("./src/templates/blogEntries")
    entry_list = []
    for blog_entry in blog_list:
        entry_list.append((url_for('blog', entry=blog_entry), blog_entry))
    return entry_list
